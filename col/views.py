import pprint
import json
import math
from copy import deepcopy

from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.views.generic import TemplateView


class BalanceView(TemplateView):
    template_name = "index.html"
    strategy = None

    def get_context_data(self, **kwargs):
        context = super(BalanceView, self).get_context_data(**kwargs)

        with open(settings.BASE_DIR+static("data.json")) as data_file:
            raw_data = json.load(data_file)

        categories = []

        for category in raw_data["categories"]:
            columns = self.strategy.balance(6, category["items"])
            categories.append({
                "columns": columns,
                "name": category["name"]
            })

        context.update({
            "categories": categories
        })

        return context
