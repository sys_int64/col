from col.strategies.base import BalanceStrategy


class TailStrategy(BalanceStrategy):
    def _balance(self, n, blocks, delta):
        columns = [[] for _ in range(0, n)]
        ci = 0

        for i, block in enumerate(blocks):
            column_weight = self._column_weight(columns[ci])

            if column_weight + len(block) > self.best_weight+delta:
                if i < len(blocks) - 1:
                    ci += 1

            ci = min(ci, n - 1)
            columns[ci].append(block)

        return columns

    def balance(self, n, items):
        items.sort()
        self.count = n
        blocks = self._make_blocks(items)
        self.best_weight = self._calc_best_weight(blocks)

        columns = None
        min_cb = 999

        for delta in range(0, -3, -1):  # Find best
            print(delta)
            c = self._balance(n, blocks,  delta)
            cb = self._calc_balance(c)

            if cb < min_cb:
                min_cb = cb
                columns = c

        return columns
