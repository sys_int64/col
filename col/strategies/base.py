import math

class BalanceStrategy(object):
    """
    Base balance strategy interface and helper methods
    """
    count = 0
    best_weight = 0

    def _calc_best_weight(self, blocks):
        s = 0

        for block in blocks:
            s += len(block)

        return math.ceil(s / self.count)

    def _column_weight(self, column):
        column_weight = 0

        for block in column:
            column_weight += len(block)

        return column_weight

    def _calc_balance(self, columns):
        max_weight = -1
        min_weight = 999

        for column in columns:
            column_weight = self._column_weight(column)

            max_weight = max(max_weight, column_weight)
            min_weight = min(min_weight, column_weight)

        return max_weight-min_weight

    def _make_blocks(self, items):
        blocks = []
        current_block = []
        current_letter = items[0][0]
        blocks.append(current_block)

        for item in items:
            if item[0] != current_letter:
                current_block = []
                current_letter = item[0]
                blocks.append(current_block)

            current_block.append(item)

        return blocks
