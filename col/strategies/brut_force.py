from copy import deepcopy

from col.strategies.base import BalanceStrategy


class BrutForceStrategy(BalanceStrategy):
    def _brute_options(self, blocks, n, columns=None):
        ci = self.count-n

        if columns is None:
            columns = [[] for _ in range(0, n)]

        if n == 0:
            return

        for i, block in enumerate(blocks):
            columns[ci].append(block)

            for j, _ in enumerate(columns[ci:]):
                if j != 0:
                    columns[ci+j] = []

            self._brute_options(blocks[i+1:], n-1, columns)

        if n == 1:
            balance = self._calc_balance(columns)

            if balance < self.min_balance:
                self.min_balance = balance
                self.columns = deepcopy(columns)

    def balance(self, n, items):
        items.sort()
        self.count = n
        blocks = self._make_blocks(items)
        self.min_balance = 999
        self.best_weight = self._calc_best_weight(blocks)
        self._brute_options(blocks, n)

        return self.columns
